BRANCH := `git branch --show-current`

default:
	just --choose

# Build a container
container:
	nix build .#container
	podman load < ./result

# Pull a container from the remote registry
pull-container:
	podman pull registry.gitlab.com/abstract-binary/abstractbinary.org:{{BRANCH}}

# Run the container
up:
	podman container rm abstractbinary.org
	podman run --rm --name abstractbinary.org -p 8080:80 "abstractbinary.org:flake"
