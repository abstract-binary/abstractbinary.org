# abstract-binary.org

Empty website deployed at https://abstractbinary.org

If you need a Nix Flake/Nginx/Gitlab CI/Kubernetes empty website
template, feel free to use this repo.  All code under MIT license.

Nix Flake
---------

You can build the container with `nix build .#container`.  See
["Building containers with Nix and Gitlab
CI"][scvalex-flake-containers].

[scvalex-flake-containers]: https://scvalex.net/posts/68/

Kubernetes
----------

Run the following to deploy to Kubernetes.  This creates a namespace,
a service, a 2-pod deployment, and a role binding for automatic
deployment (see [Triggering Kubernetes rollouts from Gitlab
CI][scvalex-ci-deployement] for details).

```
kubectl apply -f deployment.yml
```

Kubernetes Ingress (not in this repo)
-------------------------------------

You will need to add an Ingress like the following to Kubernetes as
well in order to expose the service:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: abstractbinary-org
  namespace: abstractbinary-org
  annotations:
    cert-manager.io/cluster-issuer: 'letsencrypt'
spec:
  ingressClassName: nginx
  tls:
  - hosts:
    - abstractbinary.org
    secretName: abstractbinary-org-certs
  rules:
  - host: abstractbinary.org
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: web
            port:
              number: 80
```


Gitlab CI Variables
-------------------

- `GITLAB_DEPLOYER_TOKEN`: see [Triggering Kubernetes rollouts from
        Gitlab CI][scvalex-ci-deployement],
- `CACHIX_AUTH_TOKEN`: see [Cachix docs][cachix-gitlab].

[scvalex-ci-deployments]: https://scvalex.net/posts/57/
[cachix-gitlab]: https://docs.cachix.org/continuous-integration-setup/gitlab
