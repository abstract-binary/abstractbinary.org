{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        nginxConf = pkgs.writeText "nginx.conf" ''
          user  nobody nobody;
          daemon off;
          worker_processes  auto;

          error_log  /dev/stdout info;
          pid        /dev/null;

          events {
              worker_connections  1024;
          }

          http {
              access_log    /dev/stdout;

              sendfile on;
              tcp_nopush on;
              tcp_nodelay on;
              keepalive_timeout 65;

              gzip on;
              gzip_comp_level 5;
              gzip_types
                application/atom+xml
                application/javascript
                application/json
                application/xml
                application/xml+rss
                image/svg+xml
                text/css
                text/javascript
                text/plain
                text/xml;
              gzip_vary on;

              server {
                  listen 80 default_server;

                  root "${./static}";
                  index index.html;

                  server_name localhost;

                  location / {
                      try_files $uri $uri/ =404;
                  }
              }
          }
        '';
      in
      rec {
        # `nix build`
        packages.container = pkgs.dockerTools.buildLayeredImage {
          name = "abstractbinary.org";
          tag = "flake";
          created = "now";
          contents = [ pkgs.fakeNss ];
          extraCommands = ''
            # nginx tries to create these even if it's configured not to
            mkdir -p var/log/nginx/ tmp/nginx_client_body
          '';
          config = {
            ExposedPorts = { "80/tcp" = { }; };
            Entrypoint = [ "${pkgs.nginx}/bin/nginx" ];
            Cmd = [ "-c" "${nginxConf}" ];
          };
        };

        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            just
          ];

          GIT_EXTERNAL_DIFF = "${pkgs.difftastic}/bin/difft";
        };
      }
    );
}
